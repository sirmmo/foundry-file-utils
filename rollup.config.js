import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { terser } from 'rollup-plugin-terser'
import typescript from 'rollup-plugin-typescript2'
import copy from 'rollup-plugin-copy'
import svelte from 'rollup-plugin-svelte'
import sveltePreprocess from 'svelte-preprocess'

const production = !process.env.ROLLUP_WATCH

export default [
  {
    input: 'src/index.ts',
    output: {
      dir: 'lib',
      format: 'cjs',
      exports: 'named',
      sourcemap: !production,
    },
    plugins: [
      svelte({
        preprocess: sveltePreprocess({ sourceMap: !production }),
        compilerOptions: {
          accessors: true,
          // enable run-time checks when not in production
          dev: !production,
        },
      }),
      resolve({
        browser: true,
        dedupe: ['svelte'],
      }),
      commonjs(),
      typescript({ clean: true }),

      production && terser(),
    ],
  },
  {
    input: 'src/fvtt-module.ts',
    output: {
      dir: 'dist/scripts',
      format: 'iife',
      name: 'module',
      sourcemap: !production,
    },
    plugins: [
      copy({
        targets: [{ src: 'src/module.json', dest: 'dist/' }],
      }),
      svelte({
        preprocess: sveltePreprocess({ sourceMap: !production }),
        compilerOptions: {
          // enable run-time checks when not in production
          dev: !production,
        },
      }),
      resolve({
        browser: true,
        dedupe: ['svelte'],
      }),
      commonjs(),
      typescript({ clean: true }),

      production && terser(),
    ],
  },
]

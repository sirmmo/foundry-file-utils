import { ModuleData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/packages.mjs'
import * as FileUtils from './index'

const MODULE_NAME = 'foundry-file-utils'

type ModuleType = Game.ModuleData<ModuleData> & { api?: any }

const hooks = () => {
  Hooks.on('init', () => {
    const module: ModuleType | undefined = game.modules.get(MODULE_NAME)

    if (module) {
      module.api = FileUtils
    }

    window.FileUtils = FileUtils

    Hooks.callAll('fileUtilsReady', module?.api)
    console.log(`Foundry File Utils | %cInitialized`, 'color: orange')
  })

  Hooks.on('renderSettingsConfig', (_app: any, html: string, _user: any) => {
    FileUtils.DOM.injectComponents(html)
  })
}

hooks()

/**
 * An extended FilePicker class that provides more complete data on submit
 * This picker sets the value of the linked input element to a json object
 * containing the 'path', 'activeSource', and 'bucket' of the file or directory
 */
class ExtendedFilePicker extends FilePicker {
  constructor(options = {}) {
    super(options)
  }

  field: HTMLInputElement | undefined

  _onSubmit(event: any) {
    event.preventDefault()
    const path = event.target.file.value
    const activeSource = this.activeSource
    const bucket = event.target.bucket ? event.target.bucket.value : null
    if (this.field) {
      this.field.value = JSON.stringify({ path, activeSource, bucket })
      this.field.dispatchEvent(new Event('change'))
    }

    this.close()
  }

  activateListeners(html: JQuery<HTMLElement>) {
    super.activateListeners(html)

    // remove file list if type is 'folder'
    if (this.type === 'folder') {
      $(html).find('ol.files-list').remove()
    }
  }
}

export { ExtendedFilePicker }

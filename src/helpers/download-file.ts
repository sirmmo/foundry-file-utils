import { fileFromBlob } from '../utils/file'
import { getExtensionFromContentType, getFileNameFromResponse } from '../utils/file'

/**
 * returns a file object
 * will return null if bad response, or if no filename
 * is provided and a filename cannot be infered from the request
 */
async function downloadFile(url: string, filename?: string) {
  const response = await fetch(url).catch((err) => {
    throw err
  })

  const ext = getExtensionFromContentType(response.headers.get('content-type'))
  if (!ext) {
    throw new Error('unable to infer file extension')
  }

  const fname = filename ? `${filename}.${ext}` : getFileNameFromResponse(response)
  if (!fname) {
    throw new Error('unable to infer filename')
  }

  return response?.ok ? fileFromBlob(await response.blob(), fname) : null
}

export { downloadFile }

import { DataSource } from './data-source'

/** Conditional options, if source is s3 then 'bucket' is required */
type UploadOptions<T extends DataSource> = T extends 's3'
  ? {
      bucket: string
    }
  : {
      bucket?: string
    }

export { UploadOptions }

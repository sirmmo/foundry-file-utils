import { FilePickerComponent } from '../components'
import { TYPE_PREFIX } from '../types/file-picker-types'

const injectComponents = (html: string) => {
  $(html)
    .find(`input[data-dtype^="${TYPE_PREFIX}"]`)
    .each((_i, element) => {
      // copy necessary attributes
      const target = $(element).parent()[0]
      const dtype = $(element).data('dtype')
      const name = $(element).attr('name')
      const value = $(element).val()?.toString()

      const type = dtype.split('_')[1]

      // Remove regular input
      $(element).remove()

      // Inject Svelte component
      new FilePickerComponent({
        target,
        props: {
          name,
          type,
          value: value || undefined,
        },
      })
    })
}

export { injectComponents }

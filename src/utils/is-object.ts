const isObject = (obj: unknown): obj is { [x: string]: unknown } => {
  return obj instanceof Object
}

export { isObject }

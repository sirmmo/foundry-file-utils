const isFilePickerType = (type: unknown): type is 'audio' | 'image' | 'video' | 'imagevideo' | 'folder' => {
  return type === 'folder' || type === 'image' || type === 'imagevideo' || type === 'audio' || type === 'video'
}

export { isFilePickerType }

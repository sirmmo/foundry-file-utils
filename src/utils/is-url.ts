const isUrl = (string: string) => {
  try {
    const url = new URL(string)
    return true
  } catch (_) {
    return false
  }
}

export { isUrl }
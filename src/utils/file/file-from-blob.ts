const fileFromBlob = (data: Blob, filename: string) => {
  return new File([data], filename, { type: data.type })
}

export { fileFromBlob }

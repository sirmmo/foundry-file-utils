import { getExtension, getType } from 'mime'

const getFilenameFromDisposition = (disposition: string | null): string | null => {
  return disposition ? disposition.split(';')[1].split('=')[1] : null
}

const getFilenameFromUrl = (url: string): string | null => {
  return url
    ? url
        .split('/')
        .pop()
        ?.split(/#|\?|&/)[0] ?? null
    : null
}

const getExtensionFromContentType = (mime: string | null): string | null => {
  return mime ? getExtension(mime) || null : null
}

const getFileNameFromResponse = (res: Response): string | null => {
  const fileName = getFilenameFromDisposition(res.headers.get('content-disposition'))
  if (fileName) {
    return fileName
  }

  const mime = res.headers.get('content-type')
  const ext = getExtensionFromContentType(mime)
  const urlFileName = getFilenameFromUrl(res.url) ?? ''

  if (mime && mime === getType(urlFileName)) {
    return urlFileName
  } else if (urlFileName && ext) {
    return `${urlFileName}.${ext}`
  }

  return null
}

export { getFileNameFromResponse, getExtensionFromContentType }

const fs = require('fs')

const manifestFile = './src/module.json'
const packageFile = './package.json'

const package = JSON.parse(fs.readFileSync(packageFile))
const manifest = JSON.parse(fs.readFileSync(manifestFile))

manifest.version = package.version
manifest.download = `https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/releases/${manifest.version}/downloads/foundry-file-utils-${manifest.version}.zip`

fs.writeFileSync(manifestFile, JSON.stringify(manifest, null, 2))

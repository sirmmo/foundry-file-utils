# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.7.1] - 2022-02-06

### Changed

- fixed typing of FileMeta

## [0.7.0] - 2021-11-27

### Added

- added export for fileFromBlob
- added fileFromBlob to readme

## [0.6.0] - 2021-11-27

### Changed

- remove namespace of exported members

## [0.5.1] - 2021-11-26

### Added

- added screenshot media property to manifest

### Fixed

- broken screenshot links on NPM

## [0.5.0] - 2021-11-25

### Added

- Forge support
- screenshots

### Changed

- removed file list from FilePicker of type 'folder'
- improved type safety of type constructor
- updated readme

### Fixed

- FilePicker default value error
- FilePicker component render bug

## [0.4.0] - 2021-11-23

### Changed

- updated readme

### Added

- downloadFile helper function
- uploadFile helper function
- uploadFileFromUrl helper function

## [0.3.1] - 2021-11-20

### Changed

- improved readme

## [0.3.0] - 2021-11-19

### Added

- ExtendedFilePicker class
- svelte component
- injectComponents DOM utility
- type constructor
- build process for NPM
- build process for FoundryVTT module

[0.7.1]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.7.0...0.7.1
[0.7.0]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.6.0...0.7.0
[0.6.0]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.5.1...0.6.0
[0.5.1]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.5.0...0.5.1
[0.5.0]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.4.0...0.5.0
[0.4.0]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.3.1...0.4.0
[0.3.1]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/0.3.0...0.3.1
[0.3.0]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/releases/0.3.0
